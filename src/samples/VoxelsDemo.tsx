import React, { useEffect } from 'react';
import * as THREE from 'three';
import { Box3, Vector3 } from 'three';
import { Canvas } from 'react-three-fiber';
import { InfoOverlay } from '../modules/UI/Overlay';
import { Wrapper, Controls, Lights } from './BasicDemo';
import * as MaterialCatalog from "../resources/catalogs/Materials"
import { BoxEntity } from '../modules/helpers/BoxEntityCtrlHlp';
import { useVoxelStates } from '../modules/Voxels/VoxelStates';
import { VoxelFactory } from '../modules/Voxels/VoxelFactory';
import { VoxelRenderer } from '../modules/Voxels/VoxelRenderer';
import { VoxelRenderWrapper, VoxelEntitiesWrapper } from '../modules/Voxels/VoxelWrappers';
import { VoxelModesSwitcher, VOXEL_MODES, OctreeBrowserHlp } from '../modules/Voxels/VoxelHelpers';
import { VoxelObjects } from '../modules/Voxels/VoxelObjects';
import { PointOctree } from "sparse-octree";
import { SimplexNoise } from "three/examples/jsm/math/SimplexNoise";

const Static = () => {
    sandbox();

    const incEntities = useVoxelStates(state => state.incEntities);
    useEffect(() => {
        incEntities();
    }, []);

    return (<>
        {/* <group>{grp}</group>; */}
    </>)
}

const Helpers = () => {
    const wrappers = useVoxelStates(state => state.entities.wrappers);
    const selectedEntityId = wrappers.findIndex((ent: BoxEntity) => ent.selected);
    return (<>
        <VoxelModesSwitcher initialMode={VOXEL_MODES.CREATE} />
        <OctreeBrowserHlp octree={VoxelRenderer.renderOctree} entityId={selectedEntityId} />
        {/* <VoxelHelpers size={128} /> */}
        {/* <group>{grp}</group>; */}
    </>)
}

export default ({ sample }: any) => {
    const size = 128;
    const renderBox = new Box3(new Vector3(-size, -size, -size),
        new Vector3(size, size, size));

    return (
        <>
            <InfoOverlay sample={sample} />
            <Canvas gl2 camera={{ position: [15, 30, 50] }}>
                <ambientLight intensity={0.5} />
                <Wrapper />
                <Controls />
                <Lights />
                <Static />
                <VoxelRenderWrapper renderBox={renderBox} />
                <VoxelEntitiesWrapper />
                <Helpers />
            </Canvas>
        </>
    )
};

const getMatrix: any = (posx: number, posy: number, posz: number, scale: number) => {
    var matrix = new THREE.Matrix4();
    var matScale = new THREE.Matrix4().makeScale(scale, scale, scale);
    var matTrans = new THREE.Matrix4().makeTranslation(posx, posy, posz);
    matrix.multiplyMatrices(matScale, matTrans);
    return matrix;
}

// hole in the ground
const sandbox: any = () => {
    var matlFn = () => {
        var mat: any = MaterialCatalog.ShaderTriplTexColBlend();
        mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
        // mat = MaterialCatalog.WATER();
        mat.side = THREE.BackSide;
        return mat;
    }

    var radius = 32 * 2 * Math.PI;
    var offset = 32;
    var translatMat;

    // earth = big sphere
    translatMat = getMatrix(0, -radius * 4, 0, 1);
    VoxelFactory.createEntity(VoxelObjects.sphere(radius * 4, 1 / 4, true), matlFn, translatMat);

    translatMat = getMatrix(0, 40, 0, 1);
    VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, true), matlFn, translatMat);
    // translatMat = getMatrix(0, 17, 0, 1);
    // VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);
    translatMat = getMatrix(0, 0, 0, 1);
    VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);
    // translatMat = getMatrix(-10, -10, 0, 1);
    // VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);
    // translatMat = getMatrix(-20, -20, 0, 1);
    // VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);

    // var noiseMatrix = getMatrix(0, 0, 0, 4);
    // var noiseMatrixInv = noiseMatrix.clone().getInverse(noiseMatrix);
    // var noiseBox = rangeBox.clone().applyMatrix4(noiseMatrixInv);
    // VoxelFactory.createEntity(VoxelObjects.noise(noiseBox, this.simplex), this.matlFn, noiseMatrix);
    // VoxelFactory.createRenderer(rangeBox);
}