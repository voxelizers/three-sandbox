import React from "react";
import * as THREE from "three";
import * as TextureCatalog from "./Textures";
import * as Shaders from './Shaders';
export enum CATALOG {
  WATER,
  SAND,
  SHADCOL
}
export type MaterialProps = {
  name: CATALOG,
  repeat: number
}
export function Material(props: MaterialProps) {
  switch (props.name) {
    case CATALOG.WATER:
      return (
        <meshStandardMaterial
          attach="material"
          opacity={0.5}
          transparent={true}
          color={0xff0000}
        />);
    case CATALOG.SAND:
      return (
        <meshStandardMaterial
          attach="material"
          opacity={0.5}
          transparent={false}
          color={0xffffff}
          side={THREE.DoubleSide}
          metalness={0.2}
          bumpScale={0.0005}
          map={TextureCatalog.sand(props.repeat)}
          normalMap={TextureCatalog.sand_norm(props.repeat)}
        />);
    case CATALOG.SHADCOL:
      return (<shaderMaterial
        attach="material"
        vertexShader={Shaders.Color.vertexShader}
        fragmentShader={Shaders.Color.fragmentShader}
      />)
  }
}

// TO FINISH PORTING

var WATER = () => {
  return new THREE.MeshStandardMaterial({
    opacity: 0.5,
    transparent: true,
    color: 0x2194ce
  });
};

var SAND = (repeat = 8) => {
  return new THREE.MeshStandardMaterial({
    roughness: 0.8,
    color: 0xffffff,
    side: THREE.DoubleSide,
    metalness: 0.2,
    bumpScale: 0.0005,
    map: TextureCatalog.sand(repeat),
    normalMap: TextureCatalog.sand_norm(repeat)
  });
};

var ROCK = (repeat = 8) => {
  return new THREE.MeshStandardMaterial({
    roughness: 0.8,
    color: 0xffffff,
    metalness: 0.2,
    bumpScale: 0.0005,
    map: TextureCatalog.rock(repeat),
    normalMap: TextureCatalog.rock_norm(repeat)
  });
};

var param: any = {
  dithering: true,
  wireframe: false,
  map: TextureCatalog.STARFIELD,
  side: THREE.BackSide,
  emissive: 0xffffff,
  emissiveIntensity: 0.5,
  emissiveMap: TextureCatalog.STARFIELD
}
var STARFIELD = (repeat = 8) => {
  return new THREE.MeshPhongMaterial(param);
};

var ShaderCol = () => {
  new THREE.ShaderMaterial({
    uniforms: Shaders.Color.uniforms,
    vertexShader: Shaders.Color.vertexShader,
    fragmentShader: Shaders.Color.fragmentShader,
    side: THREE.DoubleSide,
  });
}


var ShaderTriplTex = () => {
  return new THREE.ShaderMaterial({
    uniforms: Shaders.TriplanarTex.uniforms,
    vertexShader: Shaders.TriplanarTex.vertexShader,
    fragmentShader: Shaders.TriplanarTex.fragmentShader,
    side: THREE.DoubleSide,
  });
}
/**
 * Combination of triplanar texturing + color
 */
var ShaderTriplTexColBlend = () => {
  return new THREE.ShaderMaterial({
    uniforms: Shaders.TexColBlend().uniforms(new THREE.Color(0x000000)),
    vertexShader: Shaders.TexColBlend().vertexShader,
    fragmentShader: Shaders.TexColBlend().fragmentShader,
    // side: THREE.DoubleSide,
    transparent: false,
    wireframe: false
  });
}

export {
  WATER, ROCK, SAND, STARFIELD,
  ShaderCol, ShaderTriplTex, ShaderTriplTexColBlend
};

