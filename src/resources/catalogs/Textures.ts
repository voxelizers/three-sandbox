import * as THREE from 'three';
import img_sand from '../assets/img/sand.jpg';
import img_sand_norm from '../assets/img/sand_norm.png';
import img_eso_dark from '../assets/img/eso_dark.jpg'
import img_rock from '../assets/img/rock.jpg';
import img_rock_norm from '../assets/img/rock_norm.png';
import img_rock2 from '../assets/img/rock2.jpg';
import img_rock2_norm from '../assets/img/rock2_norm.png';
import img_rock2_spec from '../assets/img/rock2_spec.png';

var textureLoader = new THREE.TextureLoader();

var buildTex = (texImg: string, repeat: number) => {
    return textureLoader.load(texImg, (map: THREE.Texture) => {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(repeat, repeat);
        return map;
    });
}

var sand = (repeat: number) => { return buildTex(img_sand, repeat) };
var sand_norm = (repeat: number) => { return buildTex(img_sand_norm, repeat) };
var rock = (repeat: number) => { return buildTex(img_rock, repeat) };
var rock_norm = (repeat: number) => { return buildTex(img_rock_norm, repeat) };
var rock2 = (repeat: number) => { return buildTex(img_rock2, repeat) };
var rock2_norm = (repeat: number) => { return buildTex(img_rock2_norm, repeat) };
var rock2_spec = (repeat: number) => { return buildTex(img_rock2_spec, repeat) };
var STARFIELD = (repeat: number) => { return buildTex(img_eso_dark, repeat) };

export {sand, sand_norm, rock, rock_norm, rock2, rock2_norm, rock2_spec, STARFIELD};
