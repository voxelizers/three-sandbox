
///<reference path="../../dts/misc-types-extend.d.ts" />
import React, { useState, useEffect, useRef, useCallback } from "react";
import { useThree } from "react-three-fiber";

import { useVoxelStates } from "./VoxelStates";
import { VoxelRenderer } from "./VoxelRenderer";
import { VoxelEntity } from "./VoxelFactory";

import { OctreeHelper } from "octree-helper";
import { Octree } from "sparse-octree";
// import { Vector3, Box3, Matrix4, Sphere } from "three";
// import { VoxelObjects } from "../../legacy/voxels/VoxelObjects";
// import * as MaterialCatalog from '../../legacy/Materials'
// import { VoxelEntityWrapper } from './VoxelWrappers';
// import RaycastHlp from "../Helpers/RaycastHlp";


// modes: none, polygon, entities,
export enum VOXEL_MODES {
    SELECT,
    CREATE,
    EDIT,
    NONE
}

//    /**
//      * return all octants containing the entities at specified level
//      */
//     const getRenderAreas = (entities: VoxelEntity[], lvl: number) => {
//         var boundingSpheres = entities.map(ent => {
//             var box: Box3 = ent.dimensions.clone();
//             var sphere = new Sphere(); box.getBoundingSphere(sphere);
//             return sphere;
//         })

//         // var mat: Matrix4 = new Matrix4(); var scale: Vector3 = new Vector3(2, 2, 2);
//         // var subTrees = octree.cull(box.applyMatrix4(mat.scale(scale)))
//         const octantNodes = VoxelRenderer.octree.findNodesByLevel(lvl)
//             .filter(node => node)   // remove null
//             .filter(node => VoxelRenderer.octree.countPoints(node)) // non empty
//         // .filter(node => {
//         //     var match = boundingSpheres.find(sph => node.contains(sph.center, sph.radius*0+1))
//         //     return match;
//         // });
//         return VoxelRenderer.octree.pointCount ? octantNodes : [VoxelRenderer.octree.root];
//     }
enum ITEMS_DISP_MODE {
    SINGLE,
    FILTER,
    ALL
}

export const OctreeBrowserHlp = ({ octree, entityId }: { octree: any, entityId?: number }) => {
    // const octreeHlpRef: any = useRef();
    const [helpers, setHelpers]: any = useState([]);
    const pathRef = useRef([octree]);
    const listIdRef = useRef(0);
    const itemIdRef = useRef(0);
    const levelRef = useRef(0);
    const dataListRef: any = useRef([octree.root]);
    const filteredNodesRef: any = useRef([octree.root]);
    const applyFilterRef: any = useRef();
    const modeRef = useRef(ITEMS_DISP_MODE.FILTER);
    // const selectedItemsRef = useRef([]);

    // if (!octreeHlpRef.current) octreeHlpRef.current = new OctreeHelper(octree);

    const canvas = useThree().gl.domElement;

    // Data Lists
    const loadDataList = (listId: number) => {
        // on list change => reset everything
        if (listIdRef.current < listId) {
            dataListRef.current = [octree.root];
            levelRef.current = 0;
            listIdRef.current = listId // update ref
            console.log("Data List %s", listIdRef.current);
        }

        switch (listId) {
            case 1:
                dataListRef.current = VoxelRenderer.renderOctree.findNodesByLevel(levelRef.current);
                break;
            case 2:
                if (levelRef.current) {
                    dataListRef.current = dataListRef.current[itemIdRef.current].children
                }
                break;
            case 3:
                var iter = VoxelRenderer.renderOctree.leaves();
                var res = iter.next();
                var arr = [res.value]
                while (!res.done) {
                    res = iter.next();
                    arr.push(res.value);
                }
                dataListRef.current = arr;
                break;
            default:
                listIdRef.current = 0;  // go back to first list
                dataListRef.current = [];
        }
        // reset pointer to first elt in the list
        itemIdRef.current = 0;
        applyFilterRef.current();    // apply filter
    }

    // list filtering
    applyFilterRef.current = // useCallback(
        () => {
            filteredNodesRef.current = dataListRef.current
                .filter((node: any) => node)   // remove null items
                .filter((node: any, i: number) => modeRef.current === ITEMS_DISP_MODE.SINGLE ? (i === itemIdRef.current) : true)
                .filter((node: any) => modeRef.current === ITEMS_DISP_MODE.FILTER ? octree.countPoints(node) : true) // non empty
                .filter((node: any) => byEntityId(node))   // entity filter (if an entity is selected)

            const helpers = filteredNodesRef.current.map((node: any, i: number) => { // node to octree to helper to primitive
                var oct = new Octree(node);
                var hlp = new OctreeHelper(oct);
                return <primitive key={i} object={hlp} />
            });
            console.log("Filter: %s/%s items", filteredNodesRef.current.length, dataListRef.current.length)
            setHelpers(helpers);
        }//,[entityId])



    // entity filter: only nodes containing entity
    const byEntityId = (node: any) => {
        var voxelStatsPerEnt: any = {};
        const collectStats = (res: any) => {
            if (res.value && res.value.data) {
                var voxelsData = res.value.data;
                voxelsData.forEach(({ entities }: any) => {
                    entities.forEach((id: string | number) => {
                        voxelStatsPerEnt[id] = voxelStatsPerEnt[id] !== undefined ? voxelStatsPerEnt[id] + 1 : 1;
                    })
                })
            }
        }
        var oct = new Octree(node);
        var iter = oct.leaves();
        var res = iter.next();
        collectStats(res);

        while (!res.done) {
            res = iter.next();
            collectStats(res);
        }
        // console.log(voxelStatsPerEnt);
        return (entityId !== undefined && entityId !== -1) ? voxelStatsPerEnt[entityId] : true;
    }

    useEffect(() => {
        canvas.addEventListener('mousedown', (evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            var update = false;
            var oct = pathRef.current[pathRef.current.length - 1];
            // iterate single item
            if (evt.button === 4) {
                // getChildSubtree(itemListRef.children[itemIdRef.current]); 
                itemIdRef.current = (itemIdRef.current + 1) % dataListRef.current.length;
                applyFilterRef.current();
            }
            else if (evt.button === 3) {
                evt.preventDefault();
                evt.stopPropagation();
            }
        }, false);
        canvas.addEventListener('keydown', (evt) => {
            // toggle/change data list
            if (evt.code === "KeyD") {
                loadDataList(listIdRef.current + 1);    // load next list
            }
            // filtering mode
            if (evt.code === "KeyF") {
                var MODES_COUNT = Object.keys(ITEMS_DISP_MODE).length / 2;
                modeRef.current = (modeRef.current + 1) % MODES_COUNT;
                console.log("Mode: %s", ITEMS_DISP_MODE[modeRef.current]);
                applyFilterRef.current();
            }
            // Increase depth level in octree
            if (evt.code === "KeyL") {
                levelRef.current += 1;
                console.log("octree depth: Level %s", levelRef.current);
                loadDataList(listIdRef.current);
            }
            // render selected nodes
            if (evt.code === "KeyB") {
                // VoxelRenderer.processOctant(VoxelRenderer.renderOctree)
                filteredNodesRef.current.forEach((node: any) => {
                    VoxelRenderer.render(node);
                });
            }
        })
    }, []);

    // helpersRef.current = dispItem(0) //[<primitive key={0} object={octree} />]
    useEffect(() => {
        if (entityId !== -1)
            loadDataList(listIdRef.current)
    }, [entityId])

    return (<>
        {helpers}
    </>)
}
/**
 * inspect voxels in the octree
 */
export const VoxelInspectorHlp = ({ input }: { input: any }) => {
    // get average pos from face vertices
    if (input.face) {
        [input.face.a, input.face.b, input.face.c].forEach((faceInd, i) => {
            // const posAttr = input.object.geometry.getAttribute("position");
            // vertices.setXYZ(i, posAttr.getX(faceInd), posAttr.getY(faceInd), posAttr.getZ(faceInd));
        });
    }
    const avgPos = 0;
    // look for corresponding voxel in octree

    // display voxel box + log voxel data

    return (<></>)
}

export const VoxelModesSwitcher = ({ initialMode }: { initialMode: VOXEL_MODES }) => {
    const mode: VOXEL_MODES = useVoxelStates(state => state.mode);
    const setMode = useVoxelStates(state => state.setMode);
    const modeRef = useRef(mode);

    useEffect(() => {
        modeRef.current = mode;
    }, [mode])

    // create listener to switch between modes
    useEffect(() => {
        window.addEventListener('keydown', (evt) => {
            if (evt.code === "Tab") {
                evt.preventDefault();
                evt.stopPropagation();
                console.log("mode switched to: " + VOXEL_MODES[modeRef.current]);
                var modesCount = Object.keys(VOXEL_MODES).length / 2
                setMode((modeRef.current + 1) % modesCount);
            }
        }, false);
    }, [])

    if (mode === null) {
        setMode(initialMode);
    };

    return (<></>)
}

// export const VoxelEntityEditorHlp = ({ voxelEnt }: { voxelEnt: VoxelEntity }) => {
//     var renderer = new VoxelRenderer(voxelEnt);
//     var rangeBox = voxelEnt.dimensions
//     return (<></>)
// }


// export default () => {
//     var min = new Vector3(-92, -16, 36);
//     var max = new Vector3(-36, 80, 92);
//     var box = new Box3(min, max);
//     // box.applyMatrix4(matrix);
//     min = new Vector3(-92, -6.399999999999999, 36);
//     max = new Vector3(-36, 70.4, 92);
//     var box2 = new Box3(min, max);

//     const entitiesStatus = useVoxelStates(state => state.entities.status);
//     // var overlapBoxesArr = Object.keys(entitiesStatus).map(key => {
//     //     var overlapBoxes = Object.keys(entitiesStatus[key])
//     //         .filter(k2 => (k2 !== key) && (entitiesStatus[key][k2] !== null))
//     //         .map(k2 => { return entitiesStatus[key][k2]; });
//     //     // overlapBoxes.splice(key, 1);
//     //     // overlapBoxes = overlapBoxes.filter(box => box != null);
//     //     return overlapBoxes;
//     // });
//     // var entityBoxes = Object.keys(entitiesStatus).map(key => entitiesStatus[key][key]);
//     // console.log(entityBoxes);
//     console.log(entitiesStatus);
//     const count = useVoxelStates(state => state.entities.count)
//     console.log(count);
//     // const increase = useVoxelStates(state => state.increase);
//     // increase();
//     const mesh: any = {};

//     return (<>
//         <VoxelFactoryWrapper />
//         {/* <RaycastHlp mesh={mesh}/> */}
//     </>)
// }