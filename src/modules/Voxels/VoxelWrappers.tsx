import React, { useState, useEffect, useRef, useMemo, useCallback } from "react";
import { Matrix4, Box3 } from "three";
import { useThree } from "react-three-fiber";
import { VoxelFactory, VoxelEntity } from "./VoxelFactory";
import { VoxelRenderer } from "./VoxelRenderer";
import { useVoxelStates } from "./VoxelStates";

import { VOXEL_MODES } from "./VoxelHelpers";
import { BoxEntityCtrlHlp, BoxEntity } from "../helpers/BoxEntityCtrlHlp";
import RaycastHlp from "../helpers/RaycastHlp";
import { PointOctree } from "sparse-octree";
import { VoxelObjects } from "./VoxelObjects";
import { CATALOG } from "../../resources/catalogs/Materials";

/**
 * Wraps Voxel Objects to display their output and interact with them 
 * (control, create, update, ...)
 */

/**
 * 
 */
export const VoxelRenderWrapper = ({ renderBox }: { renderBox: Box3 }) => {
    // externalize raycater
    const setRaycast = useVoxelStates(state => state.setRaycast);
    const meshesRef = useRef([]);
    const [refreshFlag, forceRefresh] = useState(0);
    const clk = useThree().clock;
    // const renderRef: any = useRef();
    // const geomRef: any = useRef();
    // const matsRef: any = useRef([]);


    /**
     * each change to an entity in the octant area will trigger refresh.
     */
    const getLevelMeshes = (level: number) => {
        // retrieve all octree nodes at specific level
        const nodes = VoxelRenderer.renderOctree.findNodesByLevel(level);
        var meshes = nodes.filter((node: any) => node.data && node.data.geom)
            .map((node: any, i: number) => {
                const geom = node.data.geom;
                const mats = applyMat(geom);
                return <mesh key={i} args={[geom, mats]} onPointerMove={e => setRaycast(e)}>
                    {/* <bufferGeometry ref={buffGeomRef} attach="geometry">
                <bufferAttribute ref={buffAttrRef} attachObject={['attributes', 'position']} count={3} array={vertices} itemSize={3} />
            </bufferGeometry>
            <meshBasicMaterial attach="material" color="red" transparent opacity={0.4} side={THREE.DoubleSide} /> */}
                </mesh>
            })
        // return (<primitive object={octreeHelper} />)
        return meshes
    }

    const applyMat = (geom: THREE.BufferGeometry) => {
        var matFn = VoxelFactory.entities[0].materialFunction

        var matArr: THREE.Material[] = [];

        geom.groups.forEach((geomGrp, i) => {
            geomGrp.materialIndex = i;
            matArr.push(matFn(i));
        });
        // matsRef.current = [...matArr];
        return matArr;
    }

    if (!VoxelRenderer.renderOctree.pointCount && !VoxelRenderer.renderOctree.root.data) { // first rendering
        console.log("VoxelRenderer octree structure: first init");
        clk.getDelta();
        // initialize octree renderer
        VoxelRenderer.renderOctree = new PointOctree(renderBox.min, renderBox.max);
        VoxelRenderer.renderOctree.root.data = {};
        // VoxelRenderer.render(VoxelRenderer.renderOctree.root);
        Object.values(VoxelFactory.entities).forEach((entity: any) => {
            var vxSpacing = 1 / entity.voxelDef.vxdensity;
            var range = renderBox.clone().intersect(entity.dimensions);
            console.log("initial range");
            console.log(range.clone());
            range.min.floor().multiplyScalar(entity.voxelDef.vxdensity).floor().multiplyScalar(vxSpacing);
            range.max.ceil().multiplyScalar(entity.voxelDef.vxdensity).ceil().multiplyScalar(vxSpacing);
            console.log("rounded to");
            console.log(range);
            VoxelRenderer.preProcess(entity, range);
        });
        console.log("preprocessed %s entities (%s voxels) in %ss",
            VoxelFactory.entityCount, VoxelRenderer.renderOctree.pointCount, Math.round(clk.getDelta() * 100) / 100);
        VoxelRenderer.render(VoxelRenderer.renderOctree.root);
        console.log(VoxelRenderer.renderOctree);
        console.log("rendered %s voxels in %s",
            VoxelRenderer.renderOctree.pointCount, Math.round(clk.getDelta() * 100) / 100);
    }
    // first time init
    // if (!VoxelRenderer.renderOctree.pointCount) {

    // }

    meshesRef.current = getLevelMeshes(0);

    useEffect(() => {
        // create refresh listener
        window.addEventListener('keydown', (evt) => {
            if (evt.code === "KeyR") {
                forceRefresh(clk.elapsedTime);
            }
        }, false);
    }, []);


    return (
        <>
            {meshesRef.current}
            {/* <primitive object={meshRef.current} /> */}
            {/* <mesh args={[geomRef.current, matsRef.current]} onPointerMove={e => onRaycast(e)}> </mesh> */}
            {/* {mode === VOXEL_MODES.CREATE ? <RaycastHLP input={raycasted} /> : ""} */}
        </>
    );
    // return ()
}
/**
 * Helps managing entities in the VoxelFactory by creating, moving them
 * oth: Voxel creator, editor..
 */
export const VoxelEntitiesWrapper = () => {
    // external state for use with helpers (display + control)
    const wrappers = useVoxelStates(state => state.entities.wrappers);
    const setWrappers = useVoxelStates(state => state.setWrappers);
    const mode: VOXEL_MODES = useVoxelStates(state => state.mode);
    const raycast = useVoxelStates(state => state.raycast);
    const setMode = useVoxelStates(state => state.setMode);
    const modeRef = useRef(mode);

    useEffect(() => {
        modeRef.current = mode;
    }, [mode])

    /**
     *  Retrieves entities from factory (acting as the source of truth)
     *  and wrap them to control, select, ...
     */
    const getWrappers = () => {
        const wrapList = Object.keys(VoxelFactory.entities).map(i => {
            var ent = VoxelFactory.entities[i];
            // envelop to materialize entity
            const wrap = {
                box: ent.dimensions,
                selected: (wrappers && wrappers[i]) ? wrappers[i].selected : false,    //do not loose previous selection state
            }
            return wrap;
        })
        return wrapList;
    };

    const buildHelpers = (wrappers: any) => {
        const helpers = wrappers.map((wrap: BoxEntity, i: number) => {
            const hlp = <BoxEntityCtrlHlp key={i}
                boxEnt={wrap}
                onClick={(evt: MouseEvent) => { select(i, evt) }}
                onChange={(mat: Matrix4) => { move(i, mat) }}
            />;
            return hlp;
        })//.filter((h, i) => i > 0)
        // console.log("=> get wrappers")
        return helpers;
    };

    /**
     * Select boxEntity (no effect on factory)
     */
    const select = (id: string | number, e: MouseEvent) => {
        e.stopPropagation();
        // selection
        if (e.button === 1 && wrappers[id].selected !== undefined) {
            var selectState = wrappers[id].selected;
            // if (selectMode !== BOX_SELECT_MODES.MULTI) // clear selection
            wrappers.forEach((wrap: BoxEntity) => wrap.selected = wrap.selected !== undefined ? false : undefined);
            wrappers[id].selected = !selectState;
            setWrappers([...wrappers]);
        }
    }

    /**
     * Move an entity in the factory and refresh wrappers
     * @param id 
     * @param mat 
     */
    const move = (id: string | number, mat: Matrix4) => {
        VoxelFactory.entities[id].transform(mat);
        // trigger overlap check in VoxelFactory
        // VoxelFactory.detectOverlaps(id);
        // update renderer
        // VoxelFactory.detectEntities();
        // refresh.current = true;
        // setRefreshId(refreshId + 1);
        // moving entity will have effect to recalculate its box
        // wrappers needs to be fully refreshed from factory
        setWrappers(getWrappers());
    }

    /**
     * Create new entity in VoxelFactory
     */
    const create = () => {
        console.log(raycast.point);
        var matx = new Matrix4();
        matx.setPosition(raycast.point);
        var dummyObj = VoxelObjects.sphere(64 / 16, 1 / 4, false);
        // create entity in the factory
        var vxEnt = VoxelFactory.createEntity(dummyObj, CATALOG.WATER, matx);
        // setTransforms({
        //     ...transforms,
        //     [vxEnt.id]: matx // trick to rerender whole component and refresh wrappers
        // });
        setWrappers(getWrappers());
    }

    // const getSelected = () => {
    //     return entitiesRef.current ? entitiesRef.current.filter(ent => ent.selected) : [];
    //   }

    // first time init
    useEffect(() => {
        setWrappers(getWrappers());
    }, []);

    // switch to create mode
    useEffect(() => {
        window.addEventListener('keydown', (evt) => {
            if (evt.code === "KeyC" && modeRef.current !== VOXEL_MODES.CREATE)
                setMode(VOXEL_MODES.CREATE);
        }, false);
    }, [])

    const onRaycasterClick = useCallback(
        (e) => {
            e.stopPropagation();
            if (e.button === 1) {
                create();
            }
        }, [raycast.faceIndex]);

    const helpers = buildHelpers(wrappers);

    return (<>
        <group>{helpers}</group>
        {/* <group>{wrappers}</group> */}
        {mode === VOXEL_MODES.CREATE ? <RaycastHlp input={raycast} onClick={onRaycasterClick} /> : ""}
    </>)
}