///<reference path="../../dts/misc-types-extend.d.ts" />
import * as THREE from "three";
import { Box3, Color, BufferGeometry, Vector3 } from "three";
import { VoxelEntity, VoxelFactory } from "./VoxelFactory";
import { Lut } from "three/examples/jsm/math/Lut";
import { BufferGeometryUtils } from "three/examples/jsm/utils/BufferGeometryUtils";
import { PointOctree } from "sparse-octree";
import MarchingCubes from "./MarchingCubes";


// import { VoxelRenderer } from "../../legacy/voxels/VoxelRendering";

export class VoxelRenderUnit {

}

type Voxel = {
    flag: number,   // the determinant
    vertices: {},   // the output of the poligonization
    entities: [],   // the entity(ies) the voxel belongs (can be multiple in case of overlap)
    color: "",      // voxel's color
    trianglesNb: number    // faces.length
}

/**
 *  Voxel Entities Renderer
 * 
 * A renderer renders a specific zone of the Global Scalar Field by
 * requesting values to voxel entities belonging to the area.
 * 
 * A renderer holds an octree to optimize rendering.
 */

export class VoxelRenderer {
    // the render tree to optimize rendering
    // leaves contain voxels of each entities
    // a leaf can reference one or more entities in case of overlap
    static renderOctree = new PointOctree(); // initialize before use
    static currOctant: any;

    /**
     * Render an octant
     */

    static render(octant: any) {
        var area = new Box3(octant.min, octant.max);
        // Fill octant data before polygonization
        var entities: VoxelEntity[] = VoxelFactory.detectEntities(area);// check entitites present in the area
        var data: any = {
            entityIds: entities.length ? entities.map(ent => ent.id) : [],    // update octant entities
            vxSpacing: 1 / entities[0].voxelDef.vxdensity, // define voxel resolution
            isoLvl: entities[0].voxelDef.isoLvl
        }

        // console.log(octant);
        octant.data = { ...octant.data, ...data, geom: geom };
        // const { vertices, indicesGroups } = VoxelRenderer.updateRender(octant) //: VoxelRenderer.processRange(area, data); // will polygonize a whole octant
        VoxelRenderer.processNode(octant);
        const { vertices, indicesGroups } = octant.data;

        var geom: any;
        if (vertices.position.length) {
            var vertArr = vertices.position.map((v: Vector3) => {
                return v.toArray();
            });
            var lut = new Lut("rainbow", indicesGroups.length * 4);
            // var matArr: THREE.Material[] = [];
            var geomArr = indicesGroups.map((grp: number[], i: any) => {
                var geom = new THREE.BufferGeometry();
                geom.setIndex(grp);
                geom.setAttribute('position', new THREE.BufferAttribute(new Float32Array(vertArr.flat()), 3));
                geom.setAttribute('color', new THREE.BufferAttribute(new Float32Array(vertices.color.flat()), 3));
                geom.computeBoundingSphere();
                // matArr.push(this.getMaterial(i));
                return geom;
            });
            geom = BufferGeometryUtils.mergeBufferGeometries(geomArr, true);
            geom = BufferGeometryUtils.mergeVertices(geom);

            geom.computeFaceNormals();
            geom.computeVertexNormals();
            geom.computeBoundingBox();
            geom.groups.forEach((geomGrp: any, i: number) => {
                geomGrp.materialIndex = i;
            })
        }
        data.geom = geom;
        // octant.data = octant.data ? { ...octant.data, ...data, geom: geom } : { ...data, geom }
        octant.data = { ...octant.data, ...data, geom: geom };
        var polyCount = indicesGroups.reduce((sum: any, grp: string | any[]) => {
            return sum + grp.length;
        }, 0)
        // return geom;
    }

    /**
     * process a node in the render (oc)tree 
     * and compute mesh geometry
     */
    static processNode = (node: any) => {
        // data stored in node
        node.data = {
            vertices: {
                position: [],
                color: []
            },
            indicesGroups: []
        }
        const boundingBoxes: any = [];

        // get leaves with voxel count >0 and gather faces
        var iter = VoxelRenderer.renderOctree.leaves(new Box3(node.min, node.max));
        var res = iter.next();
        var voxels = res.value.data ? [...res.value.data] : []
        while (!res.done) {
            voxels.forEach((voxelData) => {
                var faces = VoxelRenderer.polygonise(voxelData.voxelization);
                var color = [1, 1, 1]; //this.voxelColFn(voxel);
                faces.forEach((face) => VoxelRenderer.addFace(face, color, node.data, boundingBoxes));
                // update voxel data
                // voxel.data = { vxColor: color, trianglesNb: faces.length } //{ entityIds: ids };
            });
            // next voxel iteration
            voxels = res.value.data ? [...res.value.data] : []
            res = iter.next();
        }
        // node now contains all vertices and indices groups
        // node.data = data;
    }

    /**
     * Generate voxel polygons using data from preprocess phase 
     * Return faces/triangles vertices
     * @param voxel 
     */
    static polygonise(voxelizationData: any) {
        var vertices: any = MarchingCubes.polygonise(voxelizationData.discriminant, voxelizationData.boundingPoints, voxelizationData.boundingVals, voxelizationData.isoLvl);
        var faces = [];
        while (vertices.length) faces.push(vertices.splice(0, 3));
        // VoxelRenderer.renderOctree.insert(voxel, voxelData);
        return faces;
    }

    /**
     * Scans the entity area for eligible voxels and insert them in the octree
     * if a voxel overlap one from another entity, it is updated or removed if no longer eligible
     * @param entity the entity to process
     * @param range optional restricted range (default range covered by entity)
     */
    static preProcess(entity: VoxelEntity, range?: Box3) {
        const rangeBox = range ? range : entity.dimensions;
        const vxSpacing = 1 / entity.voxelDef.vxdensity;
        const isoLvl = entity.voxelDef.isoLvl;
        for (var j: number = rangeBox.min.y; j < rangeBox.max.y; j += vxSpacing) {
            for (var k: number = rangeBox.min.z; k < rangeBox.max.z; k += vxSpacing) {
                // var vertSubList = [], idxSubList = [], colorSubList = [];
                for (var i: number = rangeBox.min.x; i < rangeBox.max.x; i += vxSpacing) {
                    var voxel = new THREE.Vector3(i, j, k);
                    // look for an already existing voxel in the octree
                    var voxelBis = VoxelRenderer.renderOctree.findPoints(voxel, 0)[0];
                    var entities = voxelBis ? [...voxelBis.data.entities, entity.id] : [entity.id];
                    var boundingPoints = voxelBis ? voxelBis.data.voxelization.boundingPoints : MarchingCubes.getBoundingPoints(...voxel.toArray(), vxSpacing);
                    var boundingVals = boundingPoints.map((p: any) => VoxelRenderer.getVoxelValue(entities, p));
                    var discriminant = MarchingCubes.getDiscriminant(boundingVals, isoLvl);

                    var voxelData = { entities: entities, voxelization: { boundingPoints, boundingVals, discriminant, isoLvl } } //{ entityIds: ids };
                    if (discriminant !== 0 && discriminant !== 255) {
                        if (!voxelBis) { // create a new voxel
                            VoxelRenderer.renderOctree.insert(voxel, voxelData);
                        }
                        else {
                            console.log("update existing voxel")
                            voxelBis.data = voxelData;
                        }
                        // , vxColor: color, trianglesNb: faces.length
                    } else if (voxelBis) {
                        console.log("voxel no longer in use => remove from octree")
                        VoxelRenderer.renderOctree.remove(voxelBis);
                    }
                }
            }
        }
    }

    static getVoxelValue(entityIds: number[], p: any, t = 0) {
        var ent = VoxelFactory.entities;
        var entities = entityIds.map(id => VoxelFactory.entities[id]);
        var vals = entities.map(entity => {
            var val = entity.getVoxelValue(p);
            // return val !== null ? val : 0;
            return val;
        });
        var valsFilled = entities.filter(entity => {
            return (entity.voxelDef.filled || entity.voxelDef.filled === undefined);
        }).map(function (entity) {
            var val = entity.getVoxelValue(p);
            // return val !== null ? val : 0;
            return val;
        });
        var valsEmpty = entities.filter(entity => {
            return (!entity.voxelDef.filled && entity.voxelDef.filled !== undefined);
        }).map(function (entity) {
            var val = entity.getVoxelValue(p);
            // return val !== null ? val : 0;
            return val;
        });
        var maxFilled = valsFilled.length ? Math.max(...valsFilled) : 1;
        var minEmpty = valsEmpty.length ? Math.min(...valsEmpty) : 0;
        if (!valsFilled.length)
            return minEmpty;
        else if (!valsEmpty.length)
            return maxFilled;
        else return Math.min(maxFilled, minEmpty);
    }

    static addFace = (facePos: any[], color: any, { vertices, indicesGroups }: any, boundingBoxes: Box3[]) => {
        // precheck: candidate for match
        var groupIds: any[] = [];
        boundingBoxes.forEach((box, i) => {
            if (box.distanceToPoint(facePos[0]) < 0.01 || box.distanceToPoint(facePos[1]) < 0.01 || box.distanceToPoint(facePos[2]) < 0.01)
                groupIds.push(i);
        });

        var mask: any = 0;
        var faceInd = facePos.map((vert, i) => {
            var vertIndex;
            groupIds.forEach((grpId, i) => {
                var index = VoxelRenderer.findIndexInGroup(vert, vertices, indicesGroups[grpId]);
                if (index) {
                    vertIndex = index;
                    mask += !(mask & (1 << i)) && 1 << i
                };
            });
            if (!vertIndex) {
                vertices.position.push(vert);
                vertices.color.push(color);
                vertIndex = vertices.position.length - 1;
            }
            return vertIndex;
        });

        groupIds = groupIds.reduce((acc, grpId, i) => {
            return mask & (1 << i) ? [...acc, grpId] : acc;
        }, []);

        var grpId = groupIds.length ? groupIds[0] : null;
        if (groupIds.length === 0) {
            // console.log("create");
            boundingBoxes.push(new Box3());
            indicesGroups.push([]);
            grpId = indicesGroups.length - 1
        } else if (groupIds.length >= 2) {
            VoxelRenderer.mergeGroups(groupIds, indicesGroups, boundingBoxes);
            grpId = indicesGroups.length - 1
        }
        indicesGroups[grpId].push(...faceInd);
        faceInd.forEach((ind) => boundingBoxes[grpId].expandByPoint(vertices.position[ind]));
    }

    static mergeGroups(grpIds: any[], indicesGroups: any[], boundingBoxes: Box3[]) {
        // console.log("merge");
        var mergedGrps = grpIds.reduce((grpMerge, id) => {
            var grp = indicesGroups[id];
            return [...grpMerge, ...grp]
        }, []);
        var mergedBoxes = grpIds.reduce((boxMerge, id) => {
            var box = boundingBoxes[id];
            return boxMerge.union(box)
        }, new Box3());
        grpIds.reverse().forEach(id => {
            indicesGroups.splice(id, 1);
            boundingBoxes.splice(id, 1);
        })
        indicesGroups.push(mergedGrps);
        boundingBoxes.push(mergedBoxes);
    }

    static findIndexInGroup(vert: Vector3, vertList: any, grp: any) {
        var index;
        var i = grp.length - 1;
        while (!index && i >= 0) {
            var ind = grp[i--];
            var vert2 = vertList.position[ind];
            if (vert.distanceTo(vert2) < 0.001) {
                index = ind;
            }

        }
        return index;
    }

}

// export class VoxelRenderTools {

//     face2Vx(faceIndex: number) {
//         var polygonSum = 0;
//         var voxelIndex = this.voxels.trianglesCount.findIndex((polygonCount: number) => {
//             polygonSum += polygonCount;
//             return (polygonSum > faceIndex && Math.abs(polygonSum - faceIndex) <= polygonCount);
//         });
//         if (voxelIndex !== -1)
//             return this.voxels.coords[voxelIndex].clone();//.applyMatrix4(this.matrix);
//         else {
//             console.log("undetected voxel")
//         }
//     }

//     pos2Vx(faceIndex: number) {
//         var mesh = this.mesh;
//         var faceIndices = [mesh.geometry.index.getX(faceIndex * 3),
//         mesh.geometry.index.getY(faceIndex * 3),
//         mesh.geometry.index.getZ(faceIndex * 3)];
//         var facePosArr = faceIndices.map((vertInd, i) => {
//             return new THREE.Vector3(mesh.geometry.attributes.position.getX(vertInd),
//                 mesh.geometry.attributes.position.getY(vertInd),
//                 mesh.geometry.attributes.position.getZ(vertInd));
//         });
//         // find barycentric face position by averaging its 3 vertices
//         var avgPos = facePosArr.reduce((acc, curr) => {
//             return acc.lerp(curr, 0.5);
//         });
//         // find closest voxel index
//         var voxelIndex: number; var distMinTreshold = this.vxSpacing + 0.5;
//         var minDist = this.voxels.coords.reduce((min: number, vx: Vector3, i: number) => {
//             var dist = avgPos.distanceTo(vx);
//             if (dist < min) {
//                 voxelIndex = i;
//                 return dist
//             } else return min;
//         }, 10);
//         if (minDist <= distMinTreshold) {
//             // if (minDist > 1) console.log("dist min: " + minDist);
//             return this.voxels.coords[voxelIndex].clone()/*.applyMatrix4(this.matrix)*/;
//         }
//         else {
//             console.log("undetected voxel (min dist too long): " + minDist);
//             return new THREE.Vector3(0, 0, 0);
//         }
//     }

//     vx2Blk(faceIndex: number) {
//         var mesh = this.mesh;
//         var grps = mesh.geometry.groups;
//         var index = faceIndex * 3;
//         var grpIdx = grps.findIndex((grp: { start: number; count: any; }) => {
//             return (index >= grp.start) && (index <= (grp.start + grp.count));
//         })
//         return grpIdx;
//     }

//     getPolygonFace(faceIndex: number) {
//         var mesh = this.mesh;
//         var polyGeom = mesh.geometry;
//         return new THREE.Face3(polyGeom.index.getX(faceIndex * 3), polyGeom.index.getY(faceIndex * 3), polyGeom.index.getZ(faceIndex * 3));
//     }

//     getFaceVerts(face: Face3) {
//         var mesh = this.mesh;
//         var polyPosAttr = mesh.geometry.attributes.position;
//         var faceIndexArr = [face.a, face.b, face.c];
//         var vertArr = faceIndexArr.map((vertIndex) => {
//             var vertex = new THREE.Vector3(polyPosAttr.getX(vertIndex), polyPosAttr.getY(vertIndex), polyPosAttr.getZ(vertIndex));
//             // return vertex.applyMatrix4(this.matrix).toArray();
//             return vertex.toArray();//.applyMatrix4(this.matrix).toArray();
//         });
//         return vertArr;
//     }
// }