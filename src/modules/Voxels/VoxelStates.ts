import create from "zustand";
import { VOXEL_MODES } from "./VoxelHelpers";
import RaycastHlp from "../helpers/RaycastHlp";

const [useVoxelStates] = create(set => ({
    mode: null,
    entities: {
        count: 0,
        transform: {},
        status: [],
        wrappers: []
    },
    raycast: {},
    setWrappers:
        // (rowKey: any, rowVal: any) => set(state => {
        (wrappers: any) => set(state => {
            // var entitiesStatus = { ...state.entities.status, [rowKey]: rowVal };
            // console.log(entitiesStatus);

            // var entityBoxes = Object.keys(entitiesStatus).map(key => entitiesStatus[key][key]);
            // var elt = rowVal[rowKey];
            // var entities = [...state.entities.status, elt];

            // var entities = state.entities.status+1;
            return { ...state, entities: { ...state.entities, wrappers: wrappers }/*{status: entitiesStatus}*/ }
        }),
    addEntity:
        // (rowKey: any, rowVal: any) => set(state => {
        (entWrp: any) => set(state => {
            var wrappers = [...state.entities.wrappers, entWrp]
            return { ...state, entities: { ...state.entities, wrappers: wrappers }/*{status: entitiesStatus}*/ }
        }),
    incEntities: () => set(state => ({ entities: { ...state.entities, count: state.entities.count + 1 } })),
    setMode: (mode: VOXEL_MODES) => set(state => ({ ...state, mode: mode })),
    setRaycast: (obj: any) => { set(state => { return (obj.faceIndex !== state.raycast.faceIndex) ? { ...state, raycast: obj } : {...state} }) }
}))
// export default useVoxelStates
// increase: () => set(state => ({ count: state.count + 1 })),

export { useVoxelStates };