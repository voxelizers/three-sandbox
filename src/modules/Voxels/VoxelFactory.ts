import * as THREE from 'three';
import { Box3, Vector3, Matrix4, Object3D } from 'three';
import { BoxSplitter } from '../tools/BoxUtils';
import { VoxelObjects } from './VoxelObjects';


/**
 * VoxelFactory
 * 
 * The voxel factory maintains voxel entities.
 * 
 */
class VoxelFactory {
    static entityCount = 0;
    static rendererCount = 0;
    static entities: any = {};
    static renderers = {};
    static overlaps = {};
    static entityMeshGrp = new THREE.Group();

    static createEntity(voxelObj: VoxelObjects, matlFn: any, matrix: any): VoxelEntity {
        var entityId = VoxelFactory.entityCount++;
        var voxelEntity = new VoxelEntity(entityId, voxelObj, matlFn);
        VoxelFactory.entities[entityId] = voxelEntity;
        voxelEntity.transform(matrix);
        console.log("VoxelFactory create entity: %s", entityId);
        return voxelEntity;
    }

    /**
     * used to know which entities belongs to an area of scalar field
    *  when entities moves or are added, the area could be potentially affected
    **/
    static detectEntities(areaRangeBox: Box3) {
        var entities = Object.keys(this.entities).map(key => VoxelFactory.entities[key]);

        var intersecting = entities.filter((entity, id) => {
            var entityBox = entity.dimensions; //entitiesStatus[id][id].clone();
            var overlapBox = entityBox.clone().intersect(areaRangeBox);
            // var unionBox = entityBox.clone().union(entityBox2);
            // var overlap = { entity1: entity.id, entity2: entity2.id, box: overlapBox };
            return BoxSplitter.checkIntersections(overlapBox);
        });
        // entitiesDim.forEach(entityBox => )
        return intersecting;
    }
    /**
     * entity collision detection: checks for entities colliding and returns overlap
     * @param entId 
     */
    static detectOverlaps(entId: number) {
        var boxDimArr = Object.keys(VoxelFactory.entities)
            .map(key => VoxelFactory.entities[key].dimensions);
        // boxDimArr[this.id] = entityBox;
        var overlapBoxes = boxDimArr.map((entityBox, id) => {
            var overlapBox = boxDimArr[entId].clone().intersect(entityBox);
            var unionBox = boxDimArr[entId].clone().union(entityBox);
            // var overlap = { entity1: entity.id, entity2: entity2.id, box: overlapBox };
            if (BoxSplitter.checkIntersections(overlapBox)) {
                if (entId !== id) {
                    // VoxelFactory.createRenderer(this.id, id2);
                    // store.dispatch({ type: "CREATE_GROUP", payload: { groupId:  } });
                    // console.log(store.getState().Voxels.entities.groups);
                }

                return unionBox;
                // return overlapBox;
            } else {
                return null;
            }
        })
        // entitiesDim.forEach(entityBox => )
        return overlapBoxes;
    }

    // render = () => <></>
}

/** 
 * VoxelEntity
 *
 * A Voxel Entity holds a Voxel Object parametered/instanciated with position (matrix) 
 * and dimension (boxRange) to alter the Global Scalar Field on a specfic volume.
 * 
 * If 2 Voxel Entities collides they will both share a common area of influence (overlapBox) 
 
 * The overlap is responsible for merging voxel objects.
 * 
 */

class VoxelEntity {
    id: any;
    voxelDef: any;
    transfmat: Matrix4;
    dimensions: Box3;
    ghostObj: Object3D;
    materialFunction: any;
    // mesh: any;

    constructor(entityId: any, voxelObj: any, matlFn: any) {
        this.id = entityId;
        this.voxelDef = voxelObj;
        this.transfmat = new THREE.Matrix4();
        this.dimensions = voxelObj.boundaries.clone().applyMatrix4(new THREE.Matrix4());
        this.ghostObj = new THREE.Object3D();
        this.materialFunction = matlFn;
        // this.mesh = new THREE.Mesh();
        // this.mesh.name = "entity" + this.id;
        // this.voxelRenderers[this.id] = {};
        // this.entityMeshGrp.add(entity.mesh)
        // this.voxelEntities.push(entity);
        // var dim = voxelObj.boundaries.clone().applyMatrix4(matrix);
        // store.dispatch({ type: "ADD_ENTITY", payload: { entityId: this.id } });
        // const incEntities = useVoxelStates(state => state.incEntities);
        // incEntities();
        // store.dispatch({ type: "MOVE_ENTITY", payload: { entityId: this.id, transform: matrix } });
    }
    // TODO: removed accumulation in mat provided by control helper
    transform(mat: Matrix4) {
        this.transfmat = mat;
        this.dimensions = this.voxelDef.boundaries.clone().applyMatrix4(mat);
        // console.log("applied translation: %s %s %s", mat.elements[12], mat.elements[13], mat.elements[14])
        // console.log("=> entity %s moved to position: %s %s %s", this.id, this.transfmat.elements[12], this.transfmat.elements[13], this.transfmat.elements[14])
    }

    getVoxelValue(p: Vector3, t?: number) {
        var vp = p.clone().applyMatrix4(this.transfmat.clone().getInverse(this.transfmat));
        var val = this.voxelDef.isosurface(vp);
        return val;
    }
}

function concatIds() {
    // this.id = this.entities
    //     .map(elt => elt.id)
    //     .sort(function (a, b) { return a - b })
    //     .reduce((str, id) => {
    //         return str + '.' + id;
    //     });
    var arr = Array.from(arguments);
    var ids = arr.map(elt => elt.id);
    ids.sort(function (a, b) { return a - b });
    var aggregatedId = ids.reduce((str, id) => {
        return str + '.' + id;
    });
    return aggregatedId;
}

export { VoxelFactory, VoxelEntity };
